CREATE DEFINER=`ag_prod`@`localhost` PROCEDURE `enrol_election_application_load_data`()
BEGIN

truncate table rpt_election_application_status;

-- creating temporary table to capture individual records
create temporary table Applications_Recieved (CONTRACT_ID varchar(5), PLAN_ID varchar(3),enrol_oec_source_of_truth_id bigint(20), ID  bigint(20),APPLICANT_MBI varchar(20), Applications_Recieved_Date datetime,Status varchar(50));


create temporary table Applications_Denied (  ID bigint(20),APPLICANT_MBI varchar(20),Applications_Denied_Date datetime);
create temporary table Applications_Sumbitted (ID bigint(20),APPLICANT_MBI varchar(20),Applications_Sumbitted_Date datetime);
create temporary table Applications_Cancelled_before_Submission (ID bigint(20),APPLICANT_MBI varchar(20),Applications_Cancelled_before_Submission_Date datetime);
create temporary table Applications_Cancelled_after_Acceptance (ID bigint(20),APPLICANT_MBI varchar(20),Applications_Cancelled_after_Acceptance_Date datetime);
create temporary table Applications_Disenrolled (ID bigint(20),APPLICANT_MBI varchar(20),Applications_Disenrolled_Date datetime);
create temporary table Applications_Rejected (ID bigint(20),APPLICANT_MBI varchar(20),Applications_Rejected_Date datetime);
create temporary table Applications_Accepted (ID bigint(20),APPLICANT_MBI varchar(20),Applications_Accepted_Date datetime);


-- get the records for Recieved records
INSERT INTO Applications_Recieved
select  CONTRACT_ID, PLAN_ID, enrol_oec_source_of_truth_id , NULL as ENROL_OEC_STAGING_ID, APPLICANT_MBI, insrt_dt,status from enrol_oec_source_of_truth where status = 'EA-Error' 
union 
select CONTRACT_ID, PLAN_ID, enrol_oec_source_of_truth_id, ENROL_OEC_STAGING_ID,MEDICARE_MBI as APPLICANT_MBI, insrt_dt,status from ENROL_ELECTION_APPLICATION  ;



-- get the records for denial records
insert into Applications_Denied
select eea.ENROL_OEC_STAGING_ID, eea.MEDICARE_MBI,edd.insrt_dt as deniel_date from enrol_denial_detail edd inner join enrol_election_application eea
on edd.ELECTION_APPLICATION_ID=eea.ENROL_OEC_STAGING_ID ;
-- Need to Replace ENROL_OEC_SOURCE_OF_TRUTH_ID with ENROL_OEC_STAGING_ID



-- get the records for application submiited
insert into Applications_Sumbitted
select eea.ENROL_OEC_STAGING_ID,eea.MEDICARE_MBI,emd.INSRT_DT  as application_sub_date
from enrol_election_application eea
inner join ag_case ac on eea.case_id =ac.ID 
inner join enrol_en_record eer on eer.EN_RECORD_ID = ac.ENROL_RECORD_ID
inner join enrol_marx_queue emq on emq.EN_RECORD_ID=eer.EN_RECORD_ID 
inner join enrol_marx_detail emd on emq.ENROL_MARX_QUEUE_ID = emd.MARX_QUEUE_ID 
where emq.status='SUBMITTED'
and  eea.PLAN_ID = 002
and eea.CONTRACT_ID = 'H2161' and emq.TRANSACTION_TYPE ='61';




-- get the records for application cancellilation before submission
insert into Applications_Cancelled_before_Submission
select eea.ENROL_OEC_STAGING_ID,eea.MEDICARE_MBI,ecr.REQUEST_RECEIPT_DATE as app_cancel_date_before_sub from
enrol_cancel_request ecr inner join enrol_election_application eea
on ecr.ENROL_ELECTION_APPLICATION_ID =eea.ENROL_OEC_STAGING_ID
where
ecr.CANCEL_REQUEST_STATUS = 'Cancellation Approved'  and eea.case_id not in(
select ag.id from ag_case ag
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ag.ENROL_RECORD_ID
inner join enrol_marx_queue emq
on emq.EN_RECORD_ID=eer.EN_RECORD_ID)
union
select eea.ENROL_OEC_STAGING_ID,eea.MEDICARE_MBI,ecr.REQUEST_RECEIPT_DATE as app_cancel_date_before_sub from
enrol_cancel_request ecr inner join enrol_election_application eea
on ecr.ENROL_ELECTION_APPLICATION_ID =eea.ENROL_OEC_STAGING_ID
where
ecr.CANCEL_REQUEST_STATUS = 'Cancellation Approved'  and APPROVED_BY_ENTITY='Internal' and eea.case_id  in(
select ag.id from ag_case ag
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ag.ENROL_RECORD_ID
inner join enrol_marx_queue emq
on emq.EN_RECORD_ID=eer.EN_RECORD_ID 
);




-- get the records for application cancellilation after acceptanace
insert into Applications_Cancelled_after_Acceptance
select eea.ENROL_OEC_STAGING_ID,eea.MEDICARE_MBI,ecr.REQUEST_RECEIPT_DATE as app_cancel_date_after_approval from
enrol_cancel_request ecr inner join enrol_election_application eea
on ecr.ENROL_ELECTION_APPLICATION_ID =eea.ENROL_OEC_STAGING_ID
where
ecr.CANCEL_REQUEST_STATUS = 'Cancellation Approved' and APPROVED_BY_ENTITY like 'TRC%' and eea.case_id in(
select ag.id from ag_case ag
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ag.ENROL_RECORD_ID
inner join enrol_marx_queue emq
on emq.EN_RECORD_ID=eer.EN_RECORD_ID) ;




-- get the records for disenrolled application
insert into Applications_Disenrolled
select eea.ENROL_OEC_STAGING_ID,eea.MEDICARE_MBI, etd.insrt_dt as disenroll_date
from enrol_election_application eea inner join ag_case ac
on eea.case_id=ac.id
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ac.ENROL_RECORD_ID
inner join enrol_trr_detail etd
on etd.EN_RECORD_ID=eer.EN_RECORD_ID
where etd.TRANSACTION_REPLY_CODE='013' ;




-- get the records for rejected application
insert into Applications_Rejected
select eea.ENROL_OEC_STAGING_ID,eea.MEDICARE_MBI, etd.insrt_dt as app_rej_date
from enrol_election_application eea inner join ag_case ac
on eea.case_id=ac.id
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ac.ENROL_RECORD_ID
inner join enrol_trr_detail etd
on eer.EN_RECORD_ID = etd.EN_RECORD_ID
where etd.TRANSACTION_REPLY_CODE in (select trc_code from rpt_trc where trc_type='R');




-- get the records for acceppted application
insert into Applications_Accepted
select eea.ENROL_OEC_STAGING_ID ,eea.MEDICARE_MBI, etd.insrt_dt as app_accept_date
from enrol_election_application eea inner join ag_case ac
on eea.case_id=ac.id
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ac.ENROL_RECORD_ID
inner join (select EN_RECORD_ID,max(insrt_dt) as insrt_dt,TRANSACTION_REPLY_CODE 
from enrol_trr_detail 
group by EN_RECORD_ID) etd
on eer.EN_RECORD_ID = etd.EN_RECORD_ID
where etd.TRANSACTION_REPLY_CODE='011';




-- All the members enrol_detail trc=011 insrt_dt
-- All memebers which has enrolled ,doesn't enroll_detail table ,insrt_dt from election appl

-- Final result set
insert into rpt_election_application_status 
select EST.CONTRACT_ID,EST.PLAN_ID,EST.APPLICANT_MBI as App_rec_ID,EST.Applications_Recieved_Date as App_received_date,
EST1.APPLICANT_MBI as App_Inc_ID,
Adn.APPLICANT_MBI as App_denied_ID,Adn.Applications_Denied_Date,Aps.APPLICANT_MBI as App_Sub_ID,Aps.Applications_Sumbitted_Date,Acs.APPLICANT_MBI as App_Cancel_before_sub_ID,
Acs.Applications_Cancelled_before_Submission_Date,Aca.APPLICANT_MBI as App_Cancel_after_approval_ID,Aca.Applications_Cancelled_after_Acceptance_Date,
Ad.APPLICANT_MBI as App_Disenrolled_ID,Ad.Applications_Disenrolled_Date,Ar.APPLICANT_MBI as App_rej_ID,Ar.Applications_Rejected_Date,
Aa.APPLICANT_MBI as App_accepted_ID,Aa.Applications_Accepted_Date,
enrld.MEDICARE_MBI as ENROLLED_ID, enrld.insrt_dt as ENROLLED_DATE, EST.status as Status
from Applications_Recieved EST
left join
enrol_oec_source_of_truth EST1 on EST.ENROL_OEC_SOURCE_OF_TRUTH_ID=EST1.ENROL_OEC_SOURCE_OF_TRUTH_ID and EST1.status='EA-Error'
left join Applications_Denied Adn
on Adn.ID=EST.ID
left join Applications_Sumbitted Aps
on Aps.ID=EST.ID
left join Applications_Cancelled_before_Submission Acs
On Acs.ID=EST.ID
left join Applications_Cancelled_after_Acceptance Aca
on Aca.ID=EST.ID
left join Applications_Disenrolled Ad
on Ad.ID=EST.ID
left join Applications_Rejected Ar
on Ar.Id=EST.ID
left join Applications_Accepted Aa
on Aa.ID=EST.ID
left join (select eea.ENROL_OEC_STAGING_ID ,eea.MEDICARE_MBI, etd.insrt_dt 
from enrol_election_application eea inner join ag_case ac
on eea.case_id=ac.id
inner join enrol_en_record eer
on eer.EN_RECORD_ID=ac.ENROL_RECORD_ID
inner join (select CONTRACT_NUMBER,PLAN_BENEFIT_PACKAGE_ID,EN_RECORD_ID,max(insrt_dt) as insrt_dt,TRANSACTION_REPLY_CODE 
from enrol_trr_detail 
group by CONTRACT_NUMBER,PLAN_BENEFIT_PACKAGE_ID,EN_RECORD_ID) etd
on eer.EN_RECORD_ID = etd.EN_RECORD_ID and  etd.CONTRACT_NUMBER=eea.CONTRACT_ID 
and  etd.PLAN_BENEFIT_PACKAGE_ID=eea.PLAN_ID 
where etd.TRANSACTION_REPLY_CODE='011'
) enrld on EST.ID=enrld.ENROL_OEC_STAGING_ID
where (EST.contract_id is not null and EST.contract_id <> '') and (EST.plan_id is not null and EST.plan_id <> '')
and EST.status!='Partial';





-- dropping temporary table
drop table Applications_Denied;
drop table Applications_Sumbitted;
drop table Applications_Cancelled_before_Submission;
drop table Applications_Cancelled_after_Acceptance;
drop table Applications_Disenrolled;
drop table Applications_Rejected;
drop table Applications_Accepted;
drop table Applications_Recieved;

END