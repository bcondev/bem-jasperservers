-- en_demo.vw_membership_changes source

create or replace
algorithm = UNDEFINED view `vw_membership_changes` as
select
    `rpt_election_application_status`.`CONTRACT_ID` as `CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID` as `PLAN_ID`,
    sum((case when (`rpt_election_application_status`.`APPLICATION_DISROLLED_DATE` >= date_format((now() - interval 1 day), '%Y-%m-%d')) then 1 else 0 end)) as `Disenrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` < date_format((now() - interval 1 day), '%Y-%m-%d')) then 1 else 0 end)) as `Beg_Enrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` >= date_format((now() - interval 1 day), '%Y-%m-%d')) then 1 else 0 end)) as `New_Enrolled_ID`,
    'Day' as `date_parameter`
from
    `rpt_election_application_status`
group by
    `rpt_election_application_status`.`CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID`
union
select
    `rpt_election_application_status`.`CONTRACT_ID` as `CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID` as `PLAN_ID`,
    sum((case when (`rpt_election_application_status`.`APPLICATION_DISROLLED_DATE` >= date_format((now() + interval (-(weekday(now())) - 1) day), '%Y-%m-%d')) then 1 else 0 end)) as `Disenrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` < date_format((now() + interval (-(weekday(now())) - 1) day), '%Y-%m-%d')) then 1 else 0 end)) as `Beg_Enrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` >= date_format((now() + interval (-(weekday(now())) - 1) day), '%Y-%m-%d')) then 1 else 0 end)) as `New_Enrolled_ID`,
    'Week' as `date_parameter`
from
    `rpt_election_application_status`
group by
    `rpt_election_application_status`.`CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID`
union
select
    `rpt_election_application_status`.`CONTRACT_ID` as `CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID` as `PLAN_ID`,
    sum((case when (`rpt_election_application_status`.`APPLICATION_DISROLLED_DATE` >= (curdate() - interval (dayofmonth(curdate()) - 1) day)) then 1 else 0 end)) as `Disenrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` < (curdate() - interval (dayofmonth(curdate()) - 1) day)) then 1 else 0 end)) as `Beg_Enrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` >= (curdate() - interval (dayofmonth(curdate()) - 1) day)) then 1 else 0 end)) as `New_Enrolled_ID`,
    'Month' as `date_parameter`
from
    `rpt_election_application_status`
group by
    `rpt_election_application_status`.`CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID`
union
select
    `rpt_election_application_status`.`CONTRACT_ID` as `CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID` as `PLAN_ID`,
    sum((case when (`rpt_election_application_status`.`APPLICATION_DISROLLED_DATE` between makedate(year(now()), 1) and curdate()) then 1 else 0 end)) as `Disenrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` < makedate(year(now()), 1)) then 1 else 0 end)) as `Beg_Enrolled_ID`,
    sum((case when (`rpt_election_application_status`.`ENROLLED_DATE` between makedate(year(now()), 1) and curdate()) then 1 else 0 end)) as `New_Enrolled_ID`,
    'year' as `date_parameter`
from
    `rpt_election_application_status`
group by
    `rpt_election_application_status`.`CONTRACT_ID`,
    `rpt_election_application_status`.`PLAN_ID`;