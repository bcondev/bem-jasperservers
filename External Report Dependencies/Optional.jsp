<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/spring" prefix="spring"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>

<%@ taglib uri="/spring" prefix="spring"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.jaspersoft.jasperserver.api.common.util.LocaleHelper"%>


<%--Include templates--%>
<jsp:include page="InputControlTemplates.jsp" />


<%-- input controls container --%>
<ul class="list inputControls" id="inputControlsContainer"></ul>

<script>
	 var counter=setInterval(function() {
		 if(jQuery("input[name='DATE_RANGE_option']:checked").length){
			jQuery(document.getElementById("inputControlsForm")).ready(function(){
				var radioValue = jQuery("input[name='DATE_RANGE_option']:checked").val();
				jQuery("input[type='radio']").click(function(){
					radioValue = jQuery("input[name='DATE_RANGE_option']:checked").val();
					callRadio(radioValue);
				});
			callRadio(radioValue);
			});
		clearInterval(counter);
		}
	},100);

	function callRadio(radioValue){
		if(radioValue=='Custom')
		{
			jQuery("#START_DATE").css("display","block");
			jQuery("#END_DATE").css("display","block");
			jQuery("#date_parameter").css("display","none");
		}
		else if(radioValue=='Standard'){
			jQuery("#START_DATE").css("display","none");
			jQuery("#END_DATE").css("display","none");
			jQuery("#date_parameter").css("display","block");
		}
	}

</script>
